//
//  NewsObject+CoreDataProperties.swift
//  Steam
//
//  Created by Zakhar Sidorov  on 1/31/21.
//
//

import Foundation
import CoreData


extension NewsObject {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<NewsObject> {
        return NSFetchRequest<NewsObject>(entityName: "NewsObject")
    }

    @NSManaged public var appid: Int64
    @NSManaged public var author: String?
    @NSManaged public var contents: String?
    @NSManaged public var date: Int64
    @NSManaged public var title: String?
    @NSManaged public var name: String?

}

extension NewsObject : Identifiable {

}
