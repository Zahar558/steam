//
//  SceneDelegate.swift
//  Steam
//
//  Created by Zakhar Sidorov  on 1/7/21.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        
        if let windowScene = scene as? UIWindowScene {
            let window = UIWindow(windowScene: windowScene)
            let list = GameListTableVC()
            let favs = FavsViewController()
            let news = NewsViewController()
            list.favsVC = favs
            list.newsVC = news
            
            
            list.title = "GAMES"
            favs.title = "FAVS"
            news.title = "NEWS"
            
//            let mostViewedItem = UITabBarItem.SystemItem.mostViewed
//            let favourites = UITabBarItem.SystemItem.favorites
//            let bookmarks = UITabBarItem.SystemItem.bookmarks
            
            list.tabBarItem = UITabBarItem(title: "GAMES", image: UIImage(named: "starList"), tag: 0)
            list.tabBarItem.title = "GAMES"
            favs.tabBarItem = UITabBarItem(title: "FAVS", image: UIImage(named: "star"), tag: 1)
            favs.title = "FAVS"
            news.tabBarItem = UITabBarItem(title: "NEWS", image: UIImage(named: "book"),tag: 2)
            news.title = "NEWS"
            
            
            let firstNC = UINavigationController(rootViewController: list)
            let secondNC = UINavigationController(rootViewController: favs)
            let thirdNC = UINavigationController(rootViewController: news)
            
            let tabBar = UITabBarController()
            tabBar.setViewControllers([firstNC, secondNC, thirdNC], animated: true)
            
            window.rootViewController = tabBar
            
            self.window = window
            window.makeKeyAndVisible()
            
            
        }
        
        
    }
    
    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }
    
    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }
    
    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }
    
    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }
    
    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
        
        // Save changes in the application's managed object context when the application transitions to the background.
       // DataBaseManager.shared.saveData()
    }
}

