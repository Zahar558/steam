//
//  Model.swift
//  Steam
//
//  Created by Zakhar Sidorov  on 1/8/21.
//

import Foundation

struct Empty: Codable {
    let applist: Applist
}

// MARK: - Applist
struct Applist: Codable {
    let apps: [GameModel]?
    
    
}


struct GameModel: Codable {
    var appid: Int? 
    var name: String
    
    
}
