//
//  newsModel.swift
//  Steam
//
//  Created by Zakhar Sidorov  on 1/26/21.
//

import Foundation


struct News: Codable {
    let appnews: Appnews?
}

// MARK: - Appnews
struct Appnews: Codable {
    //let appid: Int?
    let newsitems: [Newsitem]?
    let count: Int?
}

// MARK: - Newsitem

struct Newsitem: Codable {
    var name: String = "no name"
    let title: String?
    let author: String?
    let contents: String?
    let date: Int?
    let appid: Int?

    enum CodingKeys: String, CodingKey {
       // case name 
        case title
        case author, contents, date
        case appid
    }
}
