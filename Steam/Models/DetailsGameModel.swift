//
//  DetailsGameModel.swift
//  Steam
//
//  Created by Zakhar Sidorov  on 1/11/21.
//

import Foundation

struct IdTempModel: Codable {
    struct DynamicIdKey: CodingKey {
        var intValue: Int?
        var stringValue: String
        
        init?(stringValue: String) {
            self.stringValue = stringValue
        }
        
        init?(intValue: Int) {
            self.intValue = intValue
            return nil
        }
    }
    
    let model: DetailsGameTempModel?
    init(from decoder: Decoder) throws {
        let dynemicCaseContrainer = try decoder.container(keyedBy: DynamicIdKey.self )
        var model: DetailsGameTempModel? = nil
        do {
            try dynemicCaseContrainer.allKeys.forEach { (key: DynamicIdKey) in
            model = try dynemicCaseContrainer.decode(DetailsGameTempModel.self, forKey: key)
            }
        } catch let error {
            print(error)
        }
        self.model = model
    }
}

struct DetailsGameTempModel: Codable {
    let data: DetailsGameModel
    let success: Bool
}

struct DetailsGameModel: Codable {
    let header: String?
    let name: String?
    let genre: [Genre]
    let priceOverview: PriceOverView?
    let release: Release?
    let screenshots: [Screenshot]?
    let description: String?
}

extension DetailsGameModel {
    enum CodingKeys: String, CodingKey {
        case header = "header_image"
        case name = "name"
        case genre = "genres"
        case priceOverview = "price_overview"
        case release = "release_date"
        case screenshots = "screenshots"
        case description = "short_description"
    }
}


struct Genre: Codable {
    let description: String
    
}

struct PriceOverView: Codable {
    let price: String?
    let discont: Int?
}

struct Release: Codable {
    let date: String?
    
}
struct Screenshot: Codable {
    let screenshots: String?
}

extension Genre {
    enum CodingKeys: String, CodingKey {
        case description = "description"
    }
}

extension PriceOverView {
    enum CodingKeys: String, CodingKey {
        case price = "final_formatted"
        case discont = "discount_percent"
    }
}
extension Release {
    enum CodingKeys: String, CodingKey {
        case date = "date"
    }
}
extension Screenshot {
    enum CodingKeys: String, CodingKey {
        case screenshots = "path_thumbnail"
    }
}




