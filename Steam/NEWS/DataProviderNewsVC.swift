//
//  DataProviderNewsVC.swift
//  Steam
//
//  Created by Zakhar Sidorov  on 1/26/21.
//

import Foundation
import UIKit

class DataProviderNewsVC: NSObject, UITableViewDataSource {
    
    var news = [Newsitem]()
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        news.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NewsCell.identifier, for: indexPath) as! NewsCell
       // cell.nameLabel.text = "Game"
        cell.setUpWithModel(model: news[indexPath.row])
        
        
        return cell
    }
    
    
    
}
