//
//  DetailsNewsViewController.swift
//  Steam
//
//  Created by Zakhar Sidorov  on 2/3/21.
//

import UIKit

class DetailsNewsViewController: UIViewController {
    
    let scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.translatesAutoresizingMaskIntoConstraints = false
        scroll.backgroundColor = setHexColor(hex: "#102F45", alpha: 1)
        return scroll
    }()
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Gameeeeeeeeeeeeeeeeeee"
        label.numberOfLines = 0
        label.textColor = .white
        
        return label
    }()
    let authorLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Author"
        label.textColor = .white
        
        return label
    }()
    let titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "TITLE"
        label.numberOfLines = 0
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.textColor = .white
        
        return label
    }()
    let dateLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "dateeeeeeeeeeeeeee"
        label.numberOfLines = 0
        label.textColor = .white
        
        return label
    }()
    let infoLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Infooooo"
        label.numberOfLines = 0
        label.textColor = .white
        
        return label
    }()
    
    
    let separator: UIView = {
        let separator = UIView()
        separator.translatesAutoresizingMaskIntoConstraints = false
        separator.backgroundColor = .gray
    
        return separator
    }()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addViews()
        layoutSetUp()
        
    }
    
    
    func addViews() {
        view.addSubview(scrollView)
        
        scrollView.addSubview(nameLabel)
        
        scrollView.addSubview(titleLabel)
        scrollView.addSubview(authorLabel)
      //  scrollView.addSubview(dateLabel)
        scrollView.addSubview(separator)
        scrollView.addSubview(infoLabel)

    }
    
    func layoutSetUp() {
        
        scrollView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        scrollView.topAnchor.constraint(equalTo: view.topAnchor, constant: 36.0).isActive = true
        scrollView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        scrollView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        
        NSLayoutConstraint.activate([
          
            nameLabel.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 50),
            nameLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor,constant: 10),
            nameLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10),

            authorLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 15),
            authorLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 10),
            authorLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10),

            titleLabel.topAnchor.constraint(equalTo: authorLabel.bottomAnchor, constant: 15),
            titleLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 10),
            titleLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10),
            
            separator.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 10),
            separator.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 10),
            separator.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10),
            separator.heightAnchor.constraint(equalToConstant: 1),
            
            
            infoLabel.topAnchor.constraint(equalTo: separator.bottomAnchor,constant: 15),
            infoLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 10),
            infoLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10),
            infoLabel.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: -50)
            
            
             
        ])
    }
    
    
}

