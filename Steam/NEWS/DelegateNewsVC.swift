//
//  DelegateNewsVC.swift
//  Steam
//
//  Created by Zakhar Sidorov  on 1/26/21.
//

import Foundation

import UIKit

class DelegateNewsVC: NSObject, UITableViewDelegate {
    var controller: NewsViewController?
    
    var news = [Newsitem]()
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let detailsVc = DetailsNewsViewController()
        
        let info = news[indexPath.row]
        
        detailsVc.nameLabel.text = info.name
        detailsVc.authorLabel.text = info.author
        detailsVc.titleLabel.text = info.title
        detailsVc.infoLabel.text = info.contents
        
        
        controller?.navigationController?.pushViewController(detailsVc, animated: true)
        
        
        
    }
}

