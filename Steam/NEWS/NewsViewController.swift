//
//  NewsViewController.swift
//  Steam
//
//  Created by Zakhar Sidorov  on 1/7/21.
//

import UIKit

class NewsViewController: UIViewController {
    
    var manager: BasicManaging?
    
    let tableView = UITableView()
    let dataProvider = DataProviderNewsVC()
    let delegate = DelegateNewsVC()
    
    
    var newsIdArray = [Int]()
    var newsArray = [Newsitem]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        manager = DataBaseManager.shared
        delegate.controller = self
        
        setUpTableView()
        navigationSetUp()
        requestFromServer()
        loadNews()
        
    }
    
    
    private func setUpTableView() {
        
        tableView.register(NewsCell.self, forCellReuseIdentifier: NewsCell.identifier)
        tableView.delegate = delegate
        tableView.dataSource = dataProvider
        
        tableView.rowHeight =  170
        tableView.backgroundColor = setHexColor(hex: "#102F45", alpha: 1)
        
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    private func navigationSetUp() {
        navigationController?.navigationBar.barTintColor = setHexColor(hex: "#102F45", alpha: 1)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    
    func requestFromServer() {
        
        manager?.fetchCertainObjects(GameObject.self, predicate: "tagged == TRUE") { (games) in
            let taggedGames = games
            
            for id in taggedGames {
                let url = "\(API.news.rawValue)\(id.appid)"
                NetworkManager.genericFetch(urlString: url) { (news: News) in
                    guard let news = news.appnews else { return }
                    guard let items = news.newsitems else { return }
                    
                    self.manager?.cleanUpNews(completion: { (response ) in
                        switch response {
                        
                        case .success(()):
                            print("CLEAN!")
                            self.manager?.saveNews(data: items)
                            
                            
                        case .failure(let error):
                            print("failure: \(error)")
                        }
                    })
                }
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    func loadNews() {
        
        manager?.fetchData(NewsObject.self) { [weak self] (news) in
            print(news.count)
            for object in news {
                
                let model = Newsitem(name: object.name ?? "know name", title: object.title, author: object.author, contents: object.contents, date: Int(object.date), appid: Int(object.appid))
                self?.dataProvider.news.append(model)
                self?.delegate.news.append(model)

            }
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
    }
    private func convertDate(string: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateFormat = ""  //  В интернете ноль по такому формату
        guard let date = dateFormatter.date(from: string) else { return "no value" }
        let currentDate =  DateFormatter.localizedString(from: date, dateStyle: .medium, timeStyle: .short)
        return currentDate
    }
}


