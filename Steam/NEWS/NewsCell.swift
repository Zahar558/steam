//
//  NewsCell.swift
//  Steam
//
//  Created by Zakhar Sidorov  on 1/26/21.
//

import UIKit

class NewsCell: UITableViewCell {
    
    static let identifier = "NewsCell"
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Game"
        label.numberOfLines = 0
        label.textColor = .white
        
        return label
    }()
    let authorLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Author"
        label.textColor = .white
        label.numberOfLines = 0 
        
        return label
    }()
    let titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "TITLE"
        label.numberOfLines = 0
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.textColor = .white
        
        return label
    }()
    let dateLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "dateeeeeeeeeeeeeee"
        label.numberOfLines = 0
        label.textColor = .white
        
        return label
    }()
    
    let separator = UIView()
    
    
    
    override init(style: NewsCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpLayout()
        contentView.backgroundColor = setHexColor(hex: "#102F45", alpha: 1)
        //        self.backgroundColor = setHexColor(hex: "#102F45", alpha: 1)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpLayout() {
        addViews()
        layout()
        
    }
    
    
    
    func setUpWithModel(model: Newsitem) {
        guard let author = model.author else {
            return
        }
        authorLabel.text = "by \(author)"
        dateLabel.text = String(model.date!)
        titleLabel.text = model.title
        
    }
    
    func addViews() {
        contentView.addSubview(nameLabel)
        contentView.addSubview(titleLabel)
        contentView.addSubview(authorLabel)
        contentView.addSubview(dateLabel)
        contentView.addSubview(separator)
    }
    
    func layout() {
        dateLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -15).isActive = true
        dateLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10).isActive = true
        dateLabel.widthAnchor.constraint(equalToConstant: 90).isActive = true 
        
        nameLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10).isActive = true
        nameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10).isActive = true
        nameLabel.trailingAnchor.constraint(equalTo: dateLabel.leadingAnchor, constant: -10).isActive = true
        
        
        
        authorLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10).isActive = true
        authorLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor, constant: -15).isActive = true
        authorLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10).isActive = true
        
        titleLabel.topAnchor.constraint(equalTo: authorLabel.bottomAnchor, constant: 15).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10).isActive = true
        
        separator.translatesAutoresizingMaskIntoConstraints = false
        separator.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        separator.heightAnchor.constraint(equalToConstant: 1).isActive = true
        separator.backgroundColor = .gray
        separator.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10).isActive = true
        separator.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        
        
    }
}


