//
//  FavsViewController.swift
//  Steam
//
//  Created by Zakhar Sidorov  on 1/7/21.
//

import UIKit

class FavsViewController: UIViewController {
    
    var manager: BasicManaging!
    
    let tableView = UITableView()
    let dataProvider = DataProviderFavsVC()
    let delegate = DelegateFavsVC()
    let searchController = UISearchController(searchResultsController: nil)
    
    
    var id: Int = 0
    
    private var searchBarIsEmpty: Bool {
        return searchController.searchBar.text?.isEmpty ?? true
        
    }
    var isFiltering: Bool {
        return searchController.isActive && !searchBarIsEmpty
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        manager = DataBaseManager.shared
        
        tableView.dataSource = dataProvider
        setUpTableView()
        navigationSetUp()
        searchControllerSetUp()
        fetchTaggedObjects()
        // fetchList()
        
    }
    //MARK: - SetUp
    
    private func setUpTableView() {
        
        tableView.register(FavsTableViewCell.self, forCellReuseIdentifier: FavsTableViewCell.identifier)
        tableView.delegate = delegate
        tableView.dataSource = dataProvider
        tableView.rowHeight =  60
        tableView.backgroundColor = setHexColor(hex: "#102F45", alpha: 1)
        
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    private func navigationSetUp() {
        navigationController?.navigationBar.barTintColor = setHexColor(hex: "#102F45", alpha: 1)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Edit", style: .plain, target: self, action: #selector(deleteGame))
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Sort", style: .plain, target: self, action: #selector(showAlertSheet))
    }
    
    @objc func showAlertSheet() {
        
        let alert = UIAlertController(title: "Sort by" , message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Title", style: .default, handler: { (action) in
            self.dataProvider.favlist.sort(by: { $0.name! < $1.name! })
            self.tableView.reloadData()
        }))
        alert.addAction((UIAlertAction(title: "Price", style: .default, handler: { (action) in
            //            let sort = self.dataProvider.favlist
            
            
        })))
        alert.addAction((UIAlertAction(title: "Cancel", style: .cancel, handler: nil)))
        
        present(alert, animated: true)
        
    }
    
    //MARK: - Saving
    
    func fetchTaggedObjects() {
        
        if let myManager = manager {
            myManager.fetchCertainObjects(GameObject.self, predicate: "tagged == TRUE") { [weak self] (games) in
                self?.dataProvider.favlist = games
            
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    @objc func deleteGame() {
        self.tableView.isEditing = !self.tableView.isEditing
        navigationItem.rightBarButtonItem?.title = (self.tableView.isEditing) ? "Done" : "Edit"
    }
    
}
extension FavsViewController: UISearchResultsUpdating {
    
    
    private func searchControllerSetUp() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search"
        searchController.searchBar.alpha = 1
        definesPresentationContext = true
        navigationItem.searchController = searchController
        
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        dataProvider.isFiltering = isFiltering
        filterContentForSearchText(searchController.searchBar.text!)
    }
    
    private func filterContentForSearchText(_ search: String) {
        dataProvider.filtredGames = dataProvider.favlist.filter({ (game: GameObject ) -> Bool in
            guard let name = game.name else { return false }
            return (name.lowercased().contains(search.lowercased()))
        })
        tableView.reloadData()
    }
    
}

