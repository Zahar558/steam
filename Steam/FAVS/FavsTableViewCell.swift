//
//  FavsTableViewCell.swift
//  Steam
//
//  Created by Zakhar Sidorov  on 1/10/21.
//

import UIKit

class FavsTableViewCell: UITableViewCell {
    
    static let identifier = "FavsCell"
    
    
    override init(style: FavsTableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpViews()
        contentView.backgroundColor = setHexColor(hex: "#102F45", alpha: 1)
        self.backgroundColor = setHexColor(hex: "#102F45", alpha: 1)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    let nameLabel = UILabel()
    
    let priceLabel = UILabel()
    
    
    let separator = UIView()
    
    
    func setUpWithModel(model: GameObject) {
        nameLabel.text = model.name
    }
    
    func setUpViews() {
        addViews()
        layout()
        
    }
    
    
    func layout() {
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        separator.translatesAutoresizingMaskIntoConstraints = false
        priceLabel.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.text = "Name"
        nameLabel.textColor = .white
        nameLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10).isActive = true
        nameLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -50).isActive = true
        nameLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        nameLabel.numberOfLines = 0
        
        priceLabel.text = "1000"
        priceLabel.textColor = .white
        priceLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        priceLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10).isActive = true
        
        
        
        separator.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        separator.heightAnchor.constraint(equalToConstant: 1).isActive = true
        separator.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10).isActive = true
        separator.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        separator.backgroundColor = .gray
        
    }
    
    func addViews() {
        contentView.addSubview(nameLabel)
        contentView.addSubview(separator)
        contentView.addSubview(priceLabel)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
