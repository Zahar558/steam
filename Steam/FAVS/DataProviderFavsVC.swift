//
//  DataProvider.swift
//  Steam
//
//  Created by Zakhar Sidorov  on 1/10/21.
//

import Foundation
import UIKit

class DataProviderFavsVC: NSObject, UITableViewDataSource {
    
    var favlist = [GameObject]()
    var filtredGames = [GameObject]()
    
    var isFiltering: Bool?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        favlist.count
    
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FavsTableViewCell.identifier, for: indexPath) as! FavsTableViewCell
        cell.setUpWithModel(model: favlist[indexPath.row])
    
        var game: GameObject
        
        if isFiltering == true  {
            game = filtredGames[indexPath.row]
            
        } else {
            game = favlist[indexPath.row]
            
        }
        cell.nameLabel.text = game.name
        return cell
    }
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let game = favlist[sourceIndexPath.item]
        favlist.remove(at: sourceIndexPath.item)
        favlist.insert(game, at: destinationIndexPath.item)
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            let game = self.favlist[indexPath.row]
            favlist.remove(at: indexPath.item)
            tableView.deleteRows(at: [indexPath], with: .automatic)
            
            DispatchQueue.main.async {
                
                DataBaseManager.shared.deleteGame(object: game)
                tableView.reloadData ()
                
            }
        }
    }
    
    
    
}

