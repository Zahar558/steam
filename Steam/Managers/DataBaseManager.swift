//
//  DataBaseManager.swift
//  Steam
//
//  Created by Zakhar Sidorov  on 1/15/21.
//

import Foundation
import CoreData

protocol BasicManaging {
    
    func cleanUpGamesList(completion: @escaping (Result<Void,Error>) -> Void)
    
    func newSaving(data: [GameModel], onComplete: @escaping () -> Void)
    
    func fetchData<T:NSManagedObject>(_ type: T.Type, complition: @escaping ([T]) -> Void)
    
    func fetchCertainObjects<T:NSManagedObject>(_ type: T.Type, predicate: String, complition: @escaping ([T]) -> Void) 
    
    func saveNews(data: [Newsitem])
    
    func cleanUpNews(completion: @escaping (Result<Void,Error>) -> Void)
    

}



class DataBaseManager: BasicManaging {
    
    
    static let shared = DataBaseManager()
    
    lazy var managedObjectContext = persistentContainer.viewContext
    let privateMOC = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
    
    
    lazy var persistentContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: "Steam")
        
        // print(container.persistentStoreDescriptions.first?.url)
        
        
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                
                fatalError("Unresolved error \(error)")
            }
        })
        return container
    }()
    
    private init() {
        isDatabaseEmpty()
        
    }
    
    func newSaving(data: [GameModel], onComplete: @escaping () -> Void) {
        
        cleanUpGamesList { (response) in
            
            switch response {
            
            case .success(()):
                print("CLEAN!")
                
                self.privateMOC.parent = self.managedObjectContext
                
                self.privateMOC.perform {
                    
                    for game in data {
                        if let entity = NSEntityDescription.entity(forEntityName: "GameObject", in: self.privateMOC) {
                            guard let id = game.appid else {
                                return
                            }
                            let myGame = NSManagedObject(entity: entity, insertInto: self.privateMOC)
                            
                            myGame.setValue(id, forKey: "appid")
                            myGame.setValue(game.name, forKey: "name")
                        }
                    }
                    try? self.privateMOC.save()
                    self.managedObjectContext.performAndWait {
                        do {
                            try self.managedObjectContext.save()
                            onComplete()
                        } catch {
                            let nserror = error as NSError
                            fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                        }
                    }
                }
            case .failure(let error):
                print("failure: \(error)")
            }
        }
    }
    
    func saveNews(data: [Newsitem]) {
        self.privateMOC.parent = self.managedObjectContext
        
        self.privateMOC.perform {
            
            for object in data {
                if let entity = NSEntityDescription.entity(forEntityName: "NewsObject", in: self.privateMOC) {
                    
                    let news = NSManagedObject(entity: entity, insertInto: self.privateMOC)
                    news.setValue(object.name, forKey: "name")
                    news.setValue(object.appid, forKey: "appid")
                    news.setValue(object.title, forKey: "title")
                    news.setValue(object.author, forKey: "author")
                    news.setValue(object.contents, forKey: "contents")
                    news.setValue(object.date, forKey: "date")
                    
                }
            }
            
            try? self.privateMOC.save()
            self.managedObjectContext.performAndWait {
                do {
                    try self.managedObjectContext.save()
                    
                } catch {
                    let nserror = error as NSError
                    fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                }
            }
        }
    }
    
    
    
    func isDatabaseEmpty() {
        let request = NSFetchRequest<GameObject>(entityName: "GameObject")
        let requestNews =  NSFetchRequest<NewsObject>(entityName: "NewsObject")
        
        do {
            print(try managedObjectContext.count(for: request))
            print( try managedObjectContext.count(for: requestNews))
        } catch {
            print(error)
        }
    }
    
    func cleanUpGamesList(completion: @escaping (Result<Void,Error>) -> Void)  {
        
        managedObjectContext.performAndWait {
          
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "GameObject")
            //let bool = false
         request.predicate = NSPredicate(format: "tagged == FALSE")
            let deleteResult = NSBatchDeleteRequest(fetchRequest: request)
            do {
                try self.persistentContainer.persistentStoreCoordinator.execute(deleteResult, with: self.managedObjectContext)
                try self.managedObjectContext.save()
                completion(.success(()))
                
            } catch {
                print(error.localizedDescription)
                completion(.failure(error))
            }
        }
    }
    func cleanUpNews(completion: @escaping (Result<Void,Error>) -> Void)  {
        
        managedObjectContext.perform {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "NewsObject")
            let deleteResult = NSBatchDeleteRequest(fetchRequest: request)
            do {
                try self.managedObjectContext.execute(deleteResult) // тоже самое
                completion(.success(()))
               
            } catch {
                print(error.localizedDescription)
                completion(.failure(error))
            }
        }
    }
    
    func deleteGame(object: GameObject) {
        
        object.tagged = false
        
        saveContext()
    }
    
    func saveContext() {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                print("NOT SAVED")
            }
        }
    }
    
    func fetchData<T:NSManagedObject>(_ type: T.Type, complition: @escaping ([T]) -> Void) {
        let request = NSFetchRequest<T>(entityName: String(describing: type))
        
        do {
            let objects = try managedObjectContext.fetch(request)
            complition(objects)
        } catch {
            print(error)
        }
    }
    
    func fetchCertainObjects<T:NSManagedObject>(_ type: T.Type, predicate: String, complition: @escaping ([T]) -> Void) {
        let request = NSFetchRequest<T>(entityName: String(describing: type))
        //  let bool = true
        // privateMOC.parent = managedObjectContext
        
        request.predicate = NSPredicate(format: predicate)
        
        do {
            let objects = try managedObjectContext.fetch(request)
            complition(objects)
        } catch {
            print(error)
        }
    }
}

