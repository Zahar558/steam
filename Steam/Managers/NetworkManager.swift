//
//  NetworkManager.swift
//  Steam
//
//  Created by Zakhar Sidorov  on 1/11/21.
//

import Foundation

enum API: String {
    case allGames = "https://api.steampowered.com/ISteamApps/GetAppList/v2/?"
    case selectedGame = "https://store.steampowered.com/api/appdetails?appids="
    case news = "https://api.steampowered.com/ISteamNews/GetNewsForApp/v2/?appid="
    
}

protocol Manager {
    static func genericFetch<T: Decodable>(urlString: String, complition: @escaping (T) -> ())
}

class NetworkManager: Manager {
    
    static func genericFetch<T: Decodable>(urlString: String, complition: @escaping (T) -> ()) {
        
        if let url = URL(string: urlString) {
            let request = URLRequest(url: url)
            URLSession.shared.dataTask(with: request) { data, response, error in
                if let data = data {
                    do {
                        // print(String(data: data, encoding: .utf8) ?? "not a string")
                        let decoder = JSONDecoder()
                        //  decoder.keyDecodingStrategy = .convertFromSnakeCase
                        let game = try decoder.decode(T.self, from: data )
                        complition(game)
                        // print(film)
                    }
                    catch let error {
                        print("Error: \(error)")
                    }
                }
            }.resume()
        }
    }
}
