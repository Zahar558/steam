//
//  Example.swift
//  Steam
//
//  Created by Zakhar Sidorov  on 1/27/21.
//

import Foundation
//
//import CoreData
//func saveStudents(groups: [AGGroup]) {
//
//        //1 Сначала получаем контекст нашего приложения. Он автоматически создается если ставить флажок использовать кор дату при создании приложения
//guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
////
//        let managedContext = appDelegate.persistentContainer.viewContext
//
//        //2 Создаем сущность entity, которая связывает наш объект Студент из CoreData с экземпляром Контекста. Затем создаем объект и помещаем его в Контекст
//        for group in groups{
//
//            let entityGroup   = NSEntityDescription.entity(forEntityName: "Group", in: managedContext)!
//            let groupMO       = NSManagedObject(entity: entityGroup, insertInto: managedContext)
//
//            groupMO.setValue(group.name, forKey: "name")
//
//            for student in group.students{
//
//                let entityStudent = NSEntityDescription.entity(forEntityName: "Student", in: managedContext)!
//                let studentMO     = NSManagedObject(entity: entityStudent, insertInto: managedContext)
//
//                studentMO.setValue(student.firstName, forKey: "firstName")
//                studentMO.setValue(student.lastName, forKey:  "lastName")
//                studentMO.setValue(student.email, forKey:     "email")
//                studentMO.setValue(student.pathToPhoto, forKey:  "pathToPhoto")
//                studentMO.setValue(student.car, forKey:  "car")
//                studentMO.setValue(student.averageGrade, forKey:  "averageGrade")
//                studentMO.setValue(groupMO, forKey: "group")
//
//            }
//
//             //4 Сохраняем на диск данные
//             do{
//                 try managedContext.save()
//             } catch let error as NSError{
//                 print("Couldn't save. \(error), \(error.userInfo)")
//             }
//        }
//    }
