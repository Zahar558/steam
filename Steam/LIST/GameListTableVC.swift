//
//  ViewController.swift
//  Steam
//
//  Created by Zakhar Sidorov  on 1/7/21.
//

import UIKit


class GameListTableVC: UIViewController {
    
    
    var manager: BasicManaging!
    
    //MARK: - TableView
    
    let tableView = UITableView()
    let dataProviderTableView = DataProviderTableView()
    let delegateTableView = DelegateTableView()
    
    var gamesArray = [GameModel]()
    
    var favsVC: FavsViewController?
    var newsVC: NewsViewController?
    
    //MARK: - Search
    
    let searchController = UISearchController(searchResultsController: nil)
    
    private var searchBarIsEmpty: Bool {
        return searchController.searchBar.text?.isEmpty ?? true 
    }
    
    var isFiltering: Bool {
        return searchController.isActive && !searchBarIsEmpty
    }

    //MARK: - Test
    
    
    // MARK: - LIFECICLE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        manager = DataBaseManager.shared
        
        setUpTableView()
        navigationSetUp()
        searchControllerSetUp()
        requestFromServer()
        
        loadSavedData()
        
    }
    
    override func loadView() {
        super.loadView()
        
    }
    
    //MARK: -  Functions
    
    private func setUpTableView() {
        tableView.dataSource = dataProviderTableView
        tableView.delegate = delegateTableView
        self.delegateTableView.allGames = self.dataProviderTableView.allGames
        dataProviderTableView.object = self 
        delegateTableView.controller = self
        
        tableView.register(GameCell.self, forCellReuseIdentifier: GameCell.identifier)
        tableView.rowHeight =  70
        tableView.backgroundColor = setHexColor(hex: "#102F45", alpha: 1)
        
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    private func navigationSetUp() {
        navigationController?.navigationBar.barTintColor = setHexColor(hex: "#102F45", alpha: 1)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    
    private func requestFromServer() {
        NetworkManager.genericFetch(urlString: API.allGames.rawValue) { [weak self] (games: Empty)  in
            
            if let games = games.applist.apps  {
                self?.manager.newSaving(data: games, onComplete: {
                    self?.loadSavedData()
                })
            }
        }
    }
    
    func loadSavedData() {
        manager.fetchData(GameObject.self) { [weak self] (games) in
            self?.dataProviderTableView.allGames = games   /// загрузка данных
            self?.delegateTableView.allGames = games /// в таблицу
            
            
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
    }
   
    func addFaviriteGame(cell: UITableViewCell) {
        //print("WORK WORK")
        
        if let indexTapped = tableView.indexPath(for: cell) {
            print(indexTapped)

            if let favcVC = favsVC {
                var game : GameObject!
                
                if dataProviderTableView.isFiltering == true {
                    
                    game = dataProviderTableView.filtredGames[indexTapped.row]
                } else  {
                    game = dataProviderTableView.allGames[indexTapped.row]
                    
                }
                if game.tagged {
                    game.tagged = false
                } else {
                    game.tagged = true
                }
                
                DataBaseManager.shared.saveContext()
                
                guard let news = newsVC else {
                    return
                }
                news.newsIdArray.append(Int(game.appid))
                news.requestFromServer()
                news.loadNews()
                
                DispatchQueue.main.async {
                    print(favcVC.dataProvider.favlist.count)
                    favcVC.fetchTaggedObjects()
                    news.tableView.reloadData()
                }
            }
        }
    }
    
}


extension GameListTableVC: UISearchResultsUpdating {
    
    
    private func searchControllerSetUp() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search"
        searchController.searchBar.alpha = 1
        definesPresentationContext = true
        navigationItem.searchController = searchController
        
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        dataProviderTableView.isFiltering = isFiltering
        filterContentForSearchText(searchController.searchBar.text!)
    }
    
    private func filterContentForSearchText(_ search: String) {
        dataProviderTableView.filtredGames = dataProviderTableView.allGames.filter({ (game: GameObject ) -> Bool in
            guard let name = game.name else { return false }
            return (name.lowercased().contains(search.lowercased()))
        })
        tableView.reloadData()
    }
    
}


