//
//  FullSizeImageViewController.swift
//  Steam
//
//  Created by Zakhar Sidorov  on 1/21/21.
//

import UIKit

class FullSizeImageViewController: UIViewController {
    
    var fullImage = UIImageView()
    
    var download: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageSize()
        getImage()
        
    }
    
    func getImage() {
        
        guard  let url = self.download else {
            return
        }
        if let poster = URL(string: url) {
            DispatchQueue.global(qos: .utility).async {
                
                if let data = try? Data(contentsOf: poster) {
                    DispatchQueue.main.async {
                        
                        self.fullImage.image = UIImage(data: data)
                    }
                }
            }
        }
    }
    
    
    func imageSize() {
        
        view.addSubview(fullImage)
        fullImage.translatesAutoresizingMaskIntoConstraints = false
        fullImage.backgroundColor = setHexColor(hex: "#102F45", alpha: 1)
        fullImage.contentMode = .scaleAspectFit
        fullImage.frame = view.frame
    }
}

