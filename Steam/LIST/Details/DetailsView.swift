//
//  DetailsView.swift
//  Steam
//
//  Created by Zakhar Sidorov  on 1/11/21.
//

import UIKit

class DetailsView: UIView {
    
    let scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.translatesAutoresizingMaskIntoConstraints = false
        scroll.backgroundColor = setHexColor(hex: "#102F45", alpha: 1)
        
        return scroll
    }()

    let posterImageView: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.backgroundColor = setHexColor(hex: "#102F45", alpha: 1)
        image.contentMode = .scaleAspectFit
        return image
    }()
    let screenshotFirstImageView: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.backgroundColor = setHexColor(hex: "#102F45", alpha: 1)
        image.contentMode = .scaleAspectFit
        return image
    }()
    let screenshotSecondImageView: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.backgroundColor = setHexColor(hex: "#102F45", alpha: 1)
        image.contentMode = .scaleAspectFit
        return image
    }()
    let screenshotThirdImageView: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.backgroundColor = setHexColor(hex: "#102F45", alpha: 1)
        image.contentMode = .scaleAspectFit
        return image
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.backgroundColor = .clear
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 35)
        label.numberOfLines = 0
        label.text = "Title"
        return label
    }()
    let genresLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = .clear
        label.textAlignment = .center
        label.textColor = .white
        label.text = "Genres"
        return label
    }()
   
    
    let releaseLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = .clear
        label.textColor = .white
        label.text = "Release"
        return label
    }()
    
    let separator: UIView = {
        let view = UIView()
        view.backgroundColor = .gray
        return view
    }()
    
    let shortDescription: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = .clear
        label.textColor = .white
        label.text = "description"
        label.numberOfLines = 0
        return label
    }()
    
    
    func addView() {
        addSubview(scrollView)
        scrollView.addSubview(posterImageView)
        scrollView.addSubview(titleLabel)
        scrollView.addSubview(genresLabel)
        scrollView.addSubview(releaseLabel)
        scrollView.addSubview(screenshotFirstImageView)
        scrollView.addSubview(screenshotSecondImageView)
        scrollView.addSubview(shortDescription)
        scrollView.addSubview(separator)
    }
    
    func setUpConstraints() {
       
        scrollView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        scrollView.topAnchor.constraint(equalTo: topAnchor, constant: 36.0).isActive = true
        scrollView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        scrollView.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        
       
        NSLayoutConstraint.activate([
        
            posterImageView.topAnchor.constraint(equalTo: scrollView.topAnchor),
//            posterImageView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
//            posterImageView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            posterImageView.heightAnchor.constraint(equalToConstant: 300),
            posterImageView.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
            
            titleLabel.topAnchor.constraint(equalTo: posterImageView.bottomAnchor, constant: 10),
            titleLabel.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor),
            titleLabel.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
            
            genresLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor,constant: 20),
            genresLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 20),
            genresLabel.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: -20),
            
          //  genresLabel.widthAnchor.constraint(equalToConstant: 400),//
            
            releaseLabel.topAnchor.constraint(equalTo: genresLabel.bottomAnchor, constant: 10),
            releaseLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 10),
            
            shortDescription.topAnchor.constraint(equalTo: releaseLabel.bottomAnchor, constant: 20),
            shortDescription.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 10),
            shortDescription.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: -30),
            
            screenshotFirstImageView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 10),
            screenshotFirstImageView.topAnchor.constraint(equalTo: shortDescription.bottomAnchor, constant: 5),
            screenshotSecondImageView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: -10),
            screenshotFirstImageView.heightAnchor.constraint(equalToConstant: 300),
            screenshotFirstImageView.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
            
            
            
            screenshotSecondImageView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 10),
            screenshotSecondImageView.topAnchor.constraint(equalTo: screenshotFirstImageView.bottomAnchor, constant: 10),
            screenshotSecondImageView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: -10),
            screenshotSecondImageView.heightAnchor.constraint(equalToConstant: 300),
            screenshotSecondImageView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: 10),
            screenshotSecondImageView.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
        
        
        ])
    }
    
    
}

