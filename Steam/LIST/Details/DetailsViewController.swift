//
//  DetailsViewController.swift
//  Steam
//
//  Created by Zakhar Sidorov  on 1/11/21.
//

import UIKit

class DetailsViewController: UIViewController {
    
    
    let detailsView = DetailsView()
    
    var detailsModel: DetailsGameModel?
    
    let fullSizeVC = FullSizeImageViewController()
    
    
    var appID: Int = 0
    
    private var url: String  {
        get {
            return "\(API.selectedGame.rawValue)\(appID)"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpDetails()
        imageTap()
        
    }
    
    override func loadView() {
        super.loadView()
        view = detailsView
        detailsView.addView()
        detailsView.setUpConstraints()
    }
    
    func imageTap() {
        
        detailsView.posterImageView.isUserInteractionEnabled = true
        detailsView.screenshotFirstImageView.isUserInteractionEnabled = true
        detailsView.screenshotSecondImageView.isUserInteractionEnabled = true
        
        detailsView.posterImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openHeader)))
        detailsView.screenshotFirstImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openFirstScreen)))
        detailsView.screenshotSecondImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openSecondScreen)))
        
    }
    
    @objc func openHeader() {
        
        fullSizeVC.download = detailsModel?.header
        
        self.navigationController?.pushViewController(self.fullSizeVC, animated: true)
        
    }
    
    @objc func openFirstScreen() {
        guard let screenshots = detailsModel?.screenshots
        else {
            return
        }
        fullSizeVC.download = screenshots[0].screenshots
    }
    
    @objc func openSecondScreen() {
        guard let screenshots = detailsModel?.screenshots
        else {
            return
        }
        fullSizeVC.download = screenshots[1].screenshots
    }
    
    
    func setUpDetails() {
        NetworkManager.genericFetch(urlString: url) { (mygame: IdTempModel) in
            self.detailsModel = mygame.model?.data
            
            guard let title = self.detailsModel?.name,
                  let genres = self.detailsModel?.genre,
                  let header =  self.detailsModel?.header,
                  let screenshots = self.detailsModel?.screenshots,
                  let screenOne = screenshots[0].screenshots,
                  let screenTwo = screenshots[1].screenshots,
                  let shortDescription = self.detailsModel?.description,
                  let release = self.detailsModel?.release
            
            
            else { return }
            
            DispatchQueue.main.async { [self] in
                
                detailsView.titleLabel.text = title
                
                let description = genres.map(takeDescription)
                let stringGenres = description.joined(separator: "  ")
                
                detailsView.genresLabel.text = stringGenres
                detailsView.releaseLabel.text = release.date
                detailsView.shortDescription.text = shortDescription
                
                if let poster = URL(string: header), let screen = URL(string: screenOne), let screenSecond = URL(string: screenTwo)  {
                    
                    if let data = try? Data(contentsOf: poster), let dataScreen = try? Data(contentsOf: screen), let datascreenTwo = try? Data(contentsOf: screenSecond) {
                        detailsView.posterImageView.image = UIImage(data: data)
                        detailsView.screenshotFirstImageView.image = UIImage(data: dataScreen)
                        detailsView.screenshotSecondImageView.image = UIImage(data: datascreenTwo)
                        
                    }
                }
            }
        }
    }
    
    func takeDescription(genre: Genre) -> String {
        return genre.description
        
    }
}









