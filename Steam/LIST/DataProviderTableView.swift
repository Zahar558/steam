//
//  DataProvider.swift
//  Steam
//
//  Created by Zakhar Sidorov  on 1/8/21.
//

import Foundation
import UIKit

class DataProviderTableView: NSObject,UITableViewDataSource {
    
    var isFiltering: Bool?
    
    var object: GameListTableVC!
    
    var allGames = [GameObject]()
    
    var filtredGames = [GameObject]()
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering == true  {
            return filtredGames.count
        }
        return allGames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: GameCell.identifier, for: indexPath) as! GameCell
        cell.setUpWithModel(model: allGames[indexPath.row])
        
        cell.link = object
        var game: GameObject
        
        if isFiltering == true  {
            game = filtredGames[indexPath.row]
            
        } else {
            game = allGames[indexPath.row]
            
        }
        cell.nameLabel.text = game.name
        return cell
    }
}
