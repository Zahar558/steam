//
//  GameCell.swift
//  Steam
//
//  Created by Zakhar Sidorov  on 1/8/21.
//

import UIKit

class GameCell: UITableViewCell {
    
    var link: GameListTableVC?
    
    static let identifier = "GameCell"
    
    let nameLabel = UILabel()
    
    let starButton = UIButton(type: .system)
    
    let separator = UIView()
    
    override init(style: GameCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpViews()
        contentView.backgroundColor = setHexColor(hex: "#102F45", alpha: 1)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpWithModel(model: GameObject) {
        nameLabel.text = model.name
        
        if model.tagged {
            starButton.setImage(UIImage(named: "star"), for: .normal)
        } else {
            starButton.setImage(UIImage(named: "starClear"), for: .normal)
        }
    }
    
    func starButtonSetUp() {
        starButton.addTarget(self, action: #selector(addFavoriteGame), for: .touchUpInside)
    }
    
    @objc func addFavoriteGame() {
        // print("STAR STAR STAR")
        link?.addFaviriteGame(cell: self)
        
        setUpButtonImage()
    }
    
    func setUpButtonImage() {
        switch starButton.currentImage {
        case UIImage(named: "star"):
            starButton.setImage(UIImage(named: "starClear"), for: .normal)
        case UIImage(named: "starClear"):
            starButton.setImage(UIImage(named: "star"), for: .normal)
        default:
            return
        }
    }
    
    func setUpViews() {
        addViews()
        layout()
        starButtonSetUp()
    }
    
    func layout() {
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        starButton.translatesAutoresizingMaskIntoConstraints = false
        separator.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.text = "Name"
        nameLabel.textColor = .white
        nameLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10).isActive = true
        nameLabel.trailingAnchor.constraint(equalTo: starButton.leadingAnchor, constant: 20).isActive = true
        nameLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        nameLabel.numberOfLines = 0
        
        separator.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        separator.heightAnchor.constraint(equalToConstant: 1).isActive = true
        separator.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10).isActive = true
        separator.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        separator.backgroundColor = .gray
        
        starButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -8).isActive = true
        starButton.heightAnchor.constraint(equalToConstant: 65).isActive = true
        starButton.widthAnchor.constraint(equalToConstant: 65).isActive = true
        starButton.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
    }
    
    func addViews() {
        contentView.addSubview(nameLabel)
        contentView.addSubview(starButton)
        contentView.addSubview(separator)
    }
     
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
