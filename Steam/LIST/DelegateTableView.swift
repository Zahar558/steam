//
//  DelegateTableView.swift
//  Steam
//
//  Created by Zakhar Sidorov  on 1/8/21.
//

import Foundation
import UIKit

class DelegateTableView: NSObject, UITableViewDelegate {
    
    var controller: GameListTableVC?
    
    
    var allGames = [GameObject]()
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       // if indexPath.row >= games.startIndex && indexPath.row < games.endIndex {
            let detailsVc = DetailsViewController()
        
        var object: GameObject!
        
        if controller?.dataProviderTableView.isFiltering == true {
            object = controller?.dataProviderTableView.filtredGames[indexPath.row]

        } else {
            object = allGames[indexPath.row]
        }
            detailsVc.appID = Int(object.appid)
            controller?.navigationController?.pushViewController(detailsVc, animated: true)
        
        
        
    }
}
