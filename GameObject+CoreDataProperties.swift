//
//  GameObject+CoreDataProperties.swift
//  Steam
//
//  Created by Zakhar Sidorov  on 1/23/21.
//
//

import Foundation
import CoreData


extension GameObject {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<GameObject> {
        return NSFetchRequest<GameObject>(entityName: "GameObject")
    }

    @NSManaged public var appid: Int64
    @NSManaged public var name: String?
    @NSManaged public var tagged: Bool

}

extension GameObject : Identifiable {

}
